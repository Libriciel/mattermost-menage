# Menage sur Mattermost

Ce script supprime les posts, canaux public/privé, fichiers, émojis et autres éléments qui sont soit marqués comme supprimés soit plus de vieux de X secondes.

## pre-requis

Il faut un accès à la machine sur laquelle le docker gitlab est lancé.
Vous devez avoir accès aux commandes docker.

## lancement

Vous pouvez lancer le script via la commande `/usr/local/bin/mattermost-menage.sh`
Vous pouvez aussi mettre en place un `cron`, par exemple :
```bash
vi /etc/cron.d/mattermost-menage
02 6 * * * root /usr/local/bin/mattermost-menage.sh >> /var/log/mattermost-menage.log 2>&1
```

## Mise à jour de Gitlab-Mattermost

Le script contrôle la version de Mattermost attendu. Au moment de la rédaction de ce document, la version attendue est la `5.38.0`.
En cas de mise à jour du Gitlab, il est nécessaire de contrôler le bon fonctionnement du script étape par étape avant de lancer le script dans sa globalité et de changer le numéro de version attendu.