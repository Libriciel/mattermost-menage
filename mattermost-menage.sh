#!/bin/bash


# Thank's https://github.com/aljazceru/mattermost-retention/blob/master/mattermost-retention.sh

MATTERMOSTPATH="/data/gitlab/data/mattermost/data/"
MATTERMOTVERSIONCIBLE='5.38.0'
RETENTION="90"
delete_before=$(date  --date="$RETENTION day ago"  "+%s%3N")

echo "Controle de la version"
mattermostversion="$(docker exec -u gitlab-psql gitlab\_web\_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -t -c "SELECT value FROM systems WHERE name like 'Version'")"
if [ ${MATTERMOTVERSIONCIBLE} != ${mattermostversion} ]
then 
	echo "version non attendue. on ne fait rien !"
	exit
fi

echo "Changement du time out"
# Ce n'est pas tres propre mais necessaire lorsqu'il y plusieurs millions de lignes à traiter
date
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production  -c 'ALTER ROLE "gitlab-psql" SET statement_timeout=100000000'

echo "Suppression des message marque supprimé deleteat > 0"
date
docker exec -u gitlab-psql gitlab\_web\_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -c "DELETE FROM posts WHERE id IN (SELECT id FROM posts where deleteat > 0)"

echo "Suppression des emoji marqués comme supprimés"
date
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production  -c "DELETE FROM posts WHERE deleteat > 0"

echo "Suppression des bots marqués comme supprimés"
date
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production  -c "DELETE FROM bots WHERE deleteat > 0"

echo "Suppression des canaux archivés marqués comme supprimé"
date
docker exec -u gitlab-psql gitlab\_web\_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -c "DELETE FROM posts WHERE id IN (SELECT id FROM channels WHERE deleteat > 0)"
echo " - suppression des channels marqués comme supprimé"
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production  -c "DELETE FROM channels WHERE deleteat > 0"

echo "Suppression des commandes marqués comme supprimé"
date
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production  -c "DELETE FROM channels WHERE deleteat > 0"

echo "Suppression des fichiers marqués comme supprimé"
date
echo "- Recupération des infos"
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -t -c "SELECT path FROM fileinfo WHERE deleteat > 0" > /tmp/listfiletodelete.txt
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -t -c "SELECT thumbnailpath FROM fileinfo WHERE deleteat > 0" >> /tmp/listfiletodelete.txt
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -t -c "SELECT previewpath from fileinfo WHERE deleteat > 0" >> /tmp/listfiletodelete.txt

while read -r line; do
	#echo "🔎 Ligne en cours --> ${line}"
	if [ -n "$line" ]
	then
		echo "📑 ${MATTERMOSTPATH}${line}"
		shred -u "${MATTERMOSTPATH}${line}"
	fi
done < /tmp/listfiletodelete.txt


echo " - Suppression des entrées en BDD marqués comme supprimé"
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production  -c "DELETE FROM fileinfo WHERE deleteat > 0"

echo "Suppreion des incomming marqués comme supprimé"
date
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production  -c "delete from fileinfo where deleteat > 0"

echo "Suppression des ir_ marqués comme supprimé"
date
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -c "DELETE FROM ir_incident WHERE deleteat > 0"
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -c "DELETE FROM ir_playbook WHERE deleteat > 0"

echo "Suppression des reactions marqués comme supprimé"
date
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -c "DELETE FROM ir_incident WHERE deleteat > 0"
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -c "DELETE FROM ir_playbook WHERE deleteat > 0"

echo "Suppression des teammembers marqués comme supprimé"
date
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -c "DELETE FROM users WHERE deleteat > 0"


echo "---------------------------------------------------------------"
echo "On supprime les posts et fichiers > ${RETENTION} jours soit $delete_before ms"
echo "Suppresion post"
date
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -c "DELETE FROM posts WHERE id NOT IN (SELECT message FROM preferences WHERE category LIKE 'flagged_post') AND createat < $delete_before AND ispinned = false"

echo "Suppression des thread n'ayant plus lieux d'être"
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production  -c "DELETE FROM threadmemberships WHERE postid NOT IN (SELECT id FROM posts)"
echo "Suppression des fichiers"
date
echo "- Recupération des infos"
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -t -c "SELECT path FROM fileinfo WHERE postid NOT IN (SELECT id FROM posts)" > /tmp/listfiletodelete.txt
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -t -c "SELECT thumbnailpath FROM fileinfo WHERE postid NOT IN (SELECT id FROM posts)" >> /tmp/listfiletodelete.txt
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production -t -c "SELECT previewpath from fileinfo WHERE postid NOT IN (SELECT id FROM posts)" >> /tmp/listfiletodelete.txt

while read -r line; do
        #echo "🔎 Ligne en cours --> ${line}"
        if [ -n "$line" ]
        then
               echo "📑 ${MATTERMOSTPATH}${line}"
               shred -u "${MATTERMOSTPATH}${line}"
        fi
done < /tmp/listfiletodelete.txt


echo " - Suppression des entrées en BDD correspondantes"
docker exec -u gitlab-psql gitlab_web_1 psql -h /var/opt/gitlab/postgresql/ -d mattermost_production  -c "DELETE FROM fileinfo WHERE postid NOT IN (SELECT id FROM posts)"

